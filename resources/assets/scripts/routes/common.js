export default {
  init() {
    // JavaScript to be fired on the home page
      $('.dropdown-menu a.dropdown-toggle').on('click', function() {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
  }
  var $subMenu = $(this).next('.dropdown-menu');
  $subMenu.toggleClass('show');


  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function() {
    $('.dropdown-submenu .show').removeClass('show');
  });


  return false;
});

  $('.carousel-not-infinite').carousel({
    wrap: false,
  });

  // Slick Carousel
  $('.testi-slider').slick({
  accessibility:true,
  arrows: true,
  slidesToShow: 1,
  draggable:false,
  centerMode: false,
  }); 

  // Slick Carousel
  $('.slick').slick({
  accessibility:true,
  infinite: false,
  arrows: true,
  slidesToShow: 4,
  draggable:false,
  centerMode: false,
  responsive: [
    {
      breakpoint: 998,
      settings: {
        arrows: true,
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 560,
      settings: {
        arrows: true,
        slidesToShow: 1,
      },
    },
  ],
  }); 

  // More WAVs Carousel
  $('.morewavs').slick({
  accessibility:true,
  arrows: true,
  slidesToShow: 3,
  draggable:false,
  centerMode: false,
  responsive: [
    {
      breakpoint: 998,
      settings: {
        arrows: true,
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 560,
      settings: {
        arrows: true,
        slidesToShow: 1,
      },
    },
  ],
  }); 

  // Slick Carousel
  $('.layout-slider').slick({
  accessibility:true,
  infinite: false,
  arrows: true,
  slidesToShow: 2,
  draggable:false,
  centerMode: false,
  responsive: [
    {
      breakpoint: 560,
      settings: {
        arrows: true,
        slidesToShow: 1,
      },
    },
  ],
  }); 

   $('.accordion-title').on('click', function() {
        $('.accordion-title').resize(); // I do not think, you need this.
        $('.layout-slider').slick('refresh');
   });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
