@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{--@include('partials.page-header')--}}
    {{--@include('partials.content-page')--}}

    <?php if( have_rows('carousel_images') ): ?>
	  <div class="slick products" data-slick=''>
	  	Hello
	    <?php while( have_rows('carousel_images') ) : the_row(); ?>
	        <div>
	          <img src="<?php the_sub_field('carousel_images'); ?>" />
	        </div>
	    <?php  endwhile; ?>
	  </div>
	<?php endif; ?>


    @include('partials.page-builder')
  @endwhile
@endsection
