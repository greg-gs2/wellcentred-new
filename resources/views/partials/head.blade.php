<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @php wp_head() @endphp
  <?php if( have_rows('header_tracking_codes') ): ?>
    <?php while( have_rows('header_tracking_codes') ) : the_row(); ?>
        <?php get_sub_field('code'); ?>
    <?php endwhile; ?>
<?php endif; ?>
</head>
