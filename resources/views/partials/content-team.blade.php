<div class="col-lg-4 post-featured-image">
	<article @php post_class() @endphp>
		<div class="container post-cont">
				<?php if ( has_post_thumbnail()) : ?>
	    				<?php the_post_thumbnail(); ?>
				<?php endif; ?>
	           
			  <header>
			  	{{--@include('partials/entry-meta')--}}
			    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
			  </header>
			  <div class="entry-summary">
			    @php the_excerpt() @endphp
			  </div>
			  <a href="{{ get_permalink() }}" class="btn-primary btn">Read More</a>
		</div>
	</article>
</div>