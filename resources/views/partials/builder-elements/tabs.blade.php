
<?php $columns = get_sub_field('2_columns');?>

<?php if( have_rows('collapse_tabs_content') ): 
  $i = 1; // Set the increment variable ?>

<?php while ( have_rows('collapse_tabs_content') ) : the_row();?>
      
          <?php if( $columns ): ?>
              <div class="col-sm-12 col-lg-6">
            <?php else: ?>
              <div class="col-sm-12 col-lg-12">
          <?php endif; ?>

            <a class="accordion-title collapsed" data-toggle="collapse" href="#collapse<?php echo $i; ?>" role="button" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
              <h3 class="accordion-plus"><?php the_sub_field('title'); ?>
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>
              </h3>
            </a>
          <div class="accordion-content-cont collapse" id="collapse<?php echo $i; ?>">
            <div class="accordion-content">
              <?php while( has_sub_field('content') ): ?>
                  <?php if( get_sub_field('text') ): ?>
                      <?php the_sub_field('text'); ?>
                  <?php endif; ?> 
                  <?php if( get_sub_field('table_headings') ): ?>
                    <table class="pricing-table">
                       <tr>
                            <?php while( has_sub_field('table_headings') ): ?>
                                  <?php if( get_sub_field('heading_1') ): ?>
                                      <th><?php the_sub_field('heading_1'); ?></th>
                                  <?php endif; ?>
                                  <?php if( get_sub_field('heading_2') ): ?>
                                      <th><?php the_sub_field('heading_2'); ?></th>
                                  <?php endif; ?>
                                  <?php if( get_sub_field('heading_3') ): ?>
                                      <th><?php the_sub_field('heading_3'); ?></th>
                                  <?php endif; ?> 
                            <?php endwhile; ?>
                        </tr>
                        
                          <?php while( has_sub_field('table_repeater') ): ?>
                            <tr>
                                  <?php if( get_sub_field('treatment') ): ?>
                                      <td><?php the_sub_field('treatment'); ?></td>
                                  <?php endif; ?>
                                  <?php if( get_sub_field('value_1') ): ?>
                                      <td><?php the_sub_field('value_1'); ?></td>
                                  <?php endif; ?>
                                  <?php if( get_sub_field('link') ): ?>
                                      <td><a href="<?php the_sub_field('link'); ?>">Book Now</a></td>
                                  <?php endif; ?> 
                              </tr>
                            <?php endwhile; ?>
                      </table>
                  <?php endif; ?> 
              <?php endwhile; ?>
            </div>
          </div>
      </div>
<?php  $i++; // Increment the increment variable
endwhile; ?>

 <?php else :

    // no rows found

endif;

?>

