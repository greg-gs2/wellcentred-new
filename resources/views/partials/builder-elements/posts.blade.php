  <?php $news = new WP_Query(array(
      'posts_per_page'  => 4,
      'post_type'     => 'post',
      'category__not_in'  => array(5)
    )); ?>

   <?php if( $news->have_posts() ) : ?>
     <div class="container blog">
      <div class="row header">
        <div class="col-sm-12 col-lg-12">
          <h2>Latest News</h2> <a href="news/" class="button button-trans button-grey">More news</a>
        </div>
      </div>
    </div>
    <div class="container blog-feed">
      <div class="row">
                    
        <?php while( $news->have_posts() ) : ?> 
          <?php $news->the_post(); ?>
            <div class="col-sm-12 col-lg-6">
              
              <article @php post_class() @endphp>
                 <div class="row">
                      <?php if ( has_post_thumbnail()) : ?>
                          <div class="col-sm-12 col-lg-5 post-featured-image">
                            <?php the_post_thumbnail(); ?>
                          </div>
                      <?php endif; ?>
                          <div class="col-sm-12 col-lg-7">
                            <header>
                              <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
                              <p class="post-meta">@include('partials/entry-meta')</p>
                            </header>
                            <div class="entry-summary">
                              @php the_excerpt() @endphp
                            </div>
                        </div>
                  </div>
                    <a href="{{ get_permalink() }}" class="button">View Post</a>
                </article>
            </div>
        <?php endwhile; ?>

      </div>
    </div>  

    <?php endif; ?>
   <?php wp_reset_postdata(); ?>