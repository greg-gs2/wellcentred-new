<?php $news = new WP_Query(array(
  'posts_per_page'    => 1,
  'post_type'         => 'post',
  'offset'           => 0,
  'category__not_in'  => array(5)
)); ?>

  <?php if( $news->have_posts() ) : ?>
    <div class="container blog-feed2">
      <div class="row">
        <?php while( $news->have_posts() ) : ?>
          <?php $news->the_post(); ?>
          <div class="col-sm-12 col-lg-3"> 
            <article @php post_class() @endphp>
                <a href="{{ get_permalink() }}">
                  <div class="col-sm-12 col-lg-12 blog-card">
                    <div class="video-post"><svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="play-circle" class="svg-inline--fa fa-play-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M371.7 238l-176-107c-15.8-8.8-35.7 2.5-35.7 21v208c0 18.4 19.8 29.8 35.7 21l176-101c16.4-9.1 16.4-32.8 0-42zM504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zm-448 0c0-110.5 89.5-200 200-200s200 89.5 200 200-89.5 200-200 200S56 366.5 56 256z"></path></svg></div>
                    <div class="gallery-post"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="images" class="svg-inline--fa fa-images fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v208c0 44.112 35.888 80 80 80h336zm96-80V80c0-26.51-21.49-48-48-48H144c-26.51 0-48 21.49-48 48v256c0 26.51 21.49 48 48 48h384c26.51 0 48-21.49 48-48zM256 128c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-96 144l55.515-55.515c4.686-4.686 12.284-4.686 16.971 0L272 256l135.515-135.515c4.686-4.686 12.284-4.686 16.971 0L512 208v112H160v-48z"></path></svg></div>
                    <div class="blog-card-image">
                      <div class="img-cont" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                      </div>
                    </div>
                    <div class="blog-card-content">
                        <h2>{!! get_the_title() !!}</h2>
                        @include('partials/entry-meta')
                    </div>
                  </div>
                </a>
              
              </article> 
            </div>
          <?php endwhile; ?>
      </div>
    </div>
    <?php endif; ?>
<?php wp_reset_postdata(); ?>