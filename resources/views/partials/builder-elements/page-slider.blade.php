<?php $sliderid = get_sub_field('slider_id');
      $activate = get_sub_field('activate_global_text');
      $global = get_sub_field('global_text'); ?>
<?php if( have_rows('page_slider_images') ): 
  $i = 1; // Set the increment variable ?>

<div id="<?php echo $sliderid ?>" class="carousel slide carousel-fade" data-ride="carousel">

      <div class="carousel-inner">

      <?php if( $activate ): ?>
        <div class="slider-text">
          <?php echo $global ?>
        </div>
      <?php else: ?>
      <?php endif; ?>
       

      <?php while ( have_rows('page_slider_images') ) : the_row();?>
         <div class="carousel-item <?php if($i == 1) echo 'active';?> page_slider_images">
                <picture>
                  <source media="(max-width: 1024px)" srcset="<?php the_sub_field('mob_image'); ?>" title="">
                  <source media="(max-width: 768px)" srcset="<?php the_sub_field('mob_image'); ?>" title="">
                  <img src="<?php the_sub_field('image'); ?>" class="slider-image">
                </picture>
                
                  <?php if( $activate ): ?>
                  <?php else: ?>
                    <div class="slider-text">
                      <?php the_sub_field('slider_text'); ?>
                    </div>
                  <?php endif; ?>
          </div>
        <?php   $i++; // Increment the increment variable
      endwhile; ?>
      
          </div>
       <a class="carousel-control-prev" href="#<?php echo $sliderid ?>" role="button" data-slide="prev">
          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-left" class="svg-inline--fa fa-chevron-left fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#<?php echo $sliderid ?>" role="button" data-slide="next">
          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chevron-right" class="svg-inline--fa fa-chevron-right fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>
          <span class="sr-only">Next</span>
        </a>
    </div>


<?php endif; ?>
