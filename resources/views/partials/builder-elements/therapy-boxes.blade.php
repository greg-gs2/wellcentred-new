<?php if( have_rows('promo_boxes', 'options') ): ?> 
	<div class="row">
		<h3>Range Of Therapies</h3>
	</div>
	<div class="row d-lg-flex">
	    <?php while( have_rows('promo_boxes', 'options') ) : the_row(); ?>
		    <div class="promo-boxes col-12 col-lg-3">
		        <div class="promo-boxes-inner" style="background-image: url(<?php the_sub_field('background_image', 'options'); ?>);">
		          <h3><?php the_sub_field('title', 'options'); ?></h3>
		          <a href="<?php echo get_site_url(); ?><?php the_sub_field('link', 'options'); ?>" class="button">Find Our More</a>
		        </div>
		    </div>
	    <?php  endwhile; ?>
	</div>
<?php endif; ?>