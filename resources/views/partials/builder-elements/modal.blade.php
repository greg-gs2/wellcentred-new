                <?php if( get_sub_field('modal_button') ): ?>
                      <?php while( has_sub_field('modal_button') ): 
                        $title = get_sub_field('title');
                        $image = get_sub_field('image'); ?>
                            <div class="col-6 col-sm-3 col-md-3 col-lg-3 modal-button">
                              <a type="button" class="" data-toggle="modal" data-target="#<?php echo str_replace(' ', '', $title) ?>"> 
                                <img src="<?php the_sub_field('image'); ?>" /> 
                                <h3><?php the_sub_field('title'); ?> </h3>
                              </a>
                            </div>
                      <?php endwhile; ?>
                    <?php endif; ?>

                <?php if( get_sub_field('modal_pop_up') ): ?>
                  <?php while( has_sub_field('modal_pop_up') ): ?>
                    <div class="modal fade" id="<?php echo str_replace(' ', '', $title) ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $title ?>Label" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
                            </button>
                            <div class="modal-body pop-up-form">
                              <img src="<?php echo $image ?>" class="modal-image"/>
                              <?php the_sub_field('content'); ?>  
                            </div>
                          </div>
                        </div>
                      </div>
                  <?php endwhile; ?>
                <?php endif; ?>


