<?php if( have_rows('repeater_field_name') ): ?>
  <div class="slick products" data-slick=''>
    <?php while( have_rows('repeater_field_name') ) : the_row(); ?>
        <div>
          <img src="<?php the_sub_field('carousel_images'); ?>" />
        </div>
    <?php  endwhile; ?>
  </div>
<?php endif; ?>