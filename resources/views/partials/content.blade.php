<article @php post_class() @endphp>
	<div class="container post-cont">
		<div class="row d-lg-flex">
			<?php if ( has_post_thumbnail()) : ?>
                <div class="col-lg-3 post-featured-image">
    				<?php the_post_thumbnail(); ?>
    			</div>
			<?php endif; ?>
	            <div class="col-lg-9">
					  <header>
					  	{{--@include('partials/entry-meta')--}}
					    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
					  </header>
					  <div class="entry-summary">
					    @php the_excerpt() @endphp
					  </div>
					  <a href="{{ get_permalink() }}" class="btn-primary btn">View Post</a>
				</div>
		</div>
	</div>
</article>
