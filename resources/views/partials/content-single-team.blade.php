<a href="<?php echo get_post_type_archive_link( 'team' ); ?>">Back to Meet the Team</a>

<article @php post_class() @endphp>
  <header>
    <?php if ( has_post_thumbnail()) : ?>
      <div class="col-lg-12 post-featured-image">
        <?php the_post_thumbnail(); ?>
      </div>
    <?php endif; ?>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
