<?php

// Check value exists.
if( have_rows('page_content') ):

    // Loop through rows.
    while ( have_rows('page_content') ) : the_row(); ?>


      <!-- 2 Column Layout -->
        <?php if( get_row_layout() == '2_column_layout' ): ?> 

           <?php if( get_sub_field('expandable_readmore') ) : ?>
             <div class="container half-layout acf-cont readmore-button">
                  <div class="row d-lg-flex">
                    <a class="button" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Read More</a>
                  </div>
              </div>
          <?php endif?>

          <div <?php if( get_sub_field('expandable_readmore') ) : ?>id="collapse"<?php endif?> class="<?php if( get_sub_field('class') ) : ?><?php the_sub_field('class'); ?><?php endif?> container half-layout acf-cont <?php if( get_sub_field('expandable_readmore') ) : ?>collapse<?php endif?>"<?php if( get_sub_field('background_image') ) : ?>style="background-image: url(<?php the_sub_field('background_image') ?>); background-size: cover;background-position: center;"<?php  elseif( get_sub_field('background_colour') ): ?> style="background-color: <?php the_sub_field('background_colour'); ?>;"<?php endif?>>
            <div class="row d-lg-flex">
                       
                      <!-- Media Flexible content -->
                      <?php if( get_sub_field('media') ): ?>
                        <?php while( has_sub_field('media') ): ?>
                            <?php if( get_sub_field('text') ): ?>
                              <div class="col-lg-6"> 
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('pop_up_button') ): ?>
                                  <button type="button" class="button" data-toggle="modal" data-target="#contactModal">
                                    <?php the_sub_field('pop_up_button'); ?>
                                  </button>
                                  @include('partials.builder-elements.contact-modal')
                                <?php endif; ?>
                              </div>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                              <div class="col-lg-6">  
                                @include('partials.builder-elements.page-slider')
                              </div>
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                              <div class="col-lg-6"> 
                                <img src="<?php the_sub_field('image'); ?>" />
                              </div>
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="col-lg-6"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- END Media Flexible content -->

                    <!-- Media RIGHT Flexible content -->
                      <?php if( get_sub_field('media_right') ): ?>
                        <?php while( has_sub_field('media_right') ): ?>
                            <?php if( get_sub_field('text') ): ?>
                              <div class="col-lg-6"> 
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('pop_up_button') ): ?>
                                  <button type="button" class="button" data-toggle="modal" data-target="#contactModal">
                                    <?php the_sub_field('pop_up_button'); ?>
                                  </button>
                                  @include('partials.builder-elements.contact-modal')
                                <?php endif; ?>
                              </div>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?>
                              <div class="col-lg-6">  
                                @include('partials.builder-elements.page-slider')
                              </div>
                             <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                              <div class="col-lg-6"> 
                                <img src="<?php the_sub_field('image'); ?>" />
                              </div>
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                              <div class="col-lg-6"> 
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                              </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- END Media RIGHT Flexible content -->
                </div>
            </div> <!-- Flex -->
        </div>  
       <?php endif; ?>

       <!-- Full Width Layout -->
        <?php if( get_row_layout() == 'full_width_layout' ): ?> 
          <?php if( get_sub_field('expandable_readmore') ) : ?>
             <div class="container half-layout acf-cont readmore-button">
                  <div class="row d-lg-flex">
                    <a class="button" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Read More</a>
                  </div>
              </div>
          <?php endif?>

          <div <?php if( get_sub_field('expandable_readmore') ) : ?>id="collapse"<?php endif?> class="<?php if( get_sub_field('class') ) : ?><?php the_sub_field('class'); ?><?php endif?> container half-layout acf-cont <?php if( get_sub_field('expandable_readmore') ) : ?>collapse<?php endif?> <?php if( get_sub_field('full_width') ) : ?>full-width<?php endif?>"<?php if( get_sub_field('background_image') ) : ?>style="background-image: url(<?php the_sub_field('background_image') ?>); background-size: cover;background-position: center;"<?php  elseif( get_sub_field('background_colour') ): ?> style="background-color: <?php the_sub_field('background_colour'); ?>;"<?php endif?>>
            <div class="row d-lg-flex">
              <div class="col-lg-12"> 
                      <!-- Media Flexible content -->
                      <?php if( get_sub_field('media') ): ?>
                        <?php while( has_sub_field('media') ): ?>
                            <?php if( get_sub_field('text') ): ?>
                                <?php the_sub_field('text'); ?>
                                <?php if( get_sub_field('pop_up_button') ): ?>
                                  <button type="button" class="button" data-toggle="modal" data-target="#contactModal">
                                    <?php the_sub_field('pop_up_button'); ?>
                                  </button>
                                  @include('partials.builder-elements.contact-modal')
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php  if( get_row_layout() == 'page_slider' ): ?> 
                                @include('partials.builder-elements.page-slider')
                             <?php endif; ?>
                             <?php if( get_sub_field('display_therapies') ): ?>
                              @include('partials.builder-elements.therapy-boxes')
                            <?php endif; ?>
                            <?php if( get_sub_field('display_posts') ): ?>
                              @include('partials.builder-elements.posts')
                            <?php endif; ?>
                            <?php if( get_sub_field('image') ): ?>
                                <img src="<?php the_sub_field('image'); ?>" class="img-100"/>
                            <?php endif; ?>
                            <?php if( get_sub_field('youtube_video') ): ?>
                                <div class="embed-container">
                                    <div class='youtube-player' data-id='<?php the_sub_field('youtube_video'); ?>'></div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- END Media Flexible content -->
                </div>
            </div> <!-- Flex -->
        </div>  
       <?php endif; ?>



      <!-- Treatment Boxes -->
        <?php if( get_row_layout() == 'treatment_boxes' ): ?> 
          <?php if( get_sub_field('treatment_boxes') ): ?>
            <div class="container half-layout acf-cont">
                <div class="row d-lg-flex">
                  <?php while( has_sub_field('treatment_boxes') ): ?>
                    <div class="col-lg-4"> 
                      <a href="<?php the_sub_field('link'); ?>">
                        <div class="treatment-boxes" <?php if( get_sub_field('image') ): ?>style="background-image: url(<?php the_sub_field('image'); ?>);"<?php endif; ?>>
                          <h3><?php the_sub_field('title'); ?></h3>
                          <p><?php the_sub_field('sub-title'); ?></p>
                        </div>
                      </a>
                    </div>
                  <?php endwhile; ?>
              </div>
            </div>
          <?php endif; ?>  
       <?php endif; ?>

       


       <!-- Tabs -->
       <?php if( get_row_layout() == 'collapse_tabs' ): ?> 
              <div class="container acf-cont page-accordion">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <h2 class="accordion-maintitle"><?php the_sub_field('title'); ?></h2>
                </div>
                  @include('partials.builder-elements.tabs')
                
              </div>
       <?php endif; ?>


       <!-- Modal Pop Ups -->
       <?php if( get_row_layout() == 'modal_pop_up' ): ?> 
          <?php if( have_rows('modal') ): ?>
            <div class="container acf-cont carousel-con">
                <div class="row">
                    <?php while ( have_rows('modal') ) : the_row();?>
                      @include('partials.builder-elements.modal')
                    <?php endwhile; ?>
                </div>
            </div>
          <?php endif; ?>
       <?php endif; ?>


      <!-- carousel -->
       <?php if( get_row_layout() == 'carousel' ): ?> 
        <div class="container acf-cont carousel-con">
          <div class="row">

            <?php if( get_sub_field('title') ): ?>
              <div class="col-sm-12 col-lg-12 carousel-title">
                <?php the_sub_field('title'); ?>
              </div>
            <?php endif; ?>

              <?php if( have_rows('carousel_images') ): ?>
                
                  
                    <div class="col-sm-12 col-lg-12">
                          <div class="slick products" data-slick=''>
                            <?php while( have_rows('carousel_images') ) : the_row(); ?>
                                <div>
                                  <span><img src="<?php the_sub_field('image'); ?>" /></span>
                                </div>
                            <?php  endwhile; ?>
                          </div>
                      </div>
                   
                  
                <?php endif; ?>
               </div>
              </div>
       <?php endif; ?>

       <?php 
       // Case: Quote Layout.
        if( get_row_layout() == 'three_column_text' ): ?> 
              <div class="container acf-cont">
                <div class="row">
                  <div class="col-lg-12"><h2><?php the_sub_field('col_heading'); ?></h2></div>
                  <div class="col-lg-12 three-col-text"> <?php the_sub_field('text'); ?> </div>
                </div>
              </div>
       <?php endif; ?>



       

     <?php // End loop.
    endwhile;

// No value.
else : ?>
   <div class="entry-content">
      <?php the_content(); ?>
    </div>
 <?php endif; ?> 