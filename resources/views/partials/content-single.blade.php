<article @php post_class() @endphp>
  <header>
    <?php if ( has_post_thumbnail()) : ?>
      <div class="col-lg-3 post-featured-image">
        <?php the_post_thumbnail(); ?>
      </div>
    <?php endif; ?>
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    @include('partials/entry-meta')
  </header>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
</article>
