<div class="main-nav-cont">
  @include('partials.pre-header')
    <header class="banner">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-lg-2">
              <a class="brand logo" href="{{ home_url('/') }}">
                {{-- @if(is_page( 'Home' )) --}}
                <!-- <img src="<?php //the_field('brand_logo_alternative', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}"> -->
                {{-- @else --}}
                <img src="<?php the_field('logo', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}">
                {{-- @endif --}} 
              </a>
          </div>
          <div class="col-sm-8 col-lg-10 align-right">
                <button class="navbar-toggler mob-nav-con hide-desktop" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false"
                        aria-label="Toggle navigation">
                  <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
                </button>
                <nav class="navbar navbar-expand-lg hide-mobile" role="navigation">
                    <div class="collapse navbar-collapse nav-primary ml-auto nav-right"  id="navbarSupportedContent">
                      @if (has_nav_menu('primary_navigation'))
                        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'depth' => 4, 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'navbar-nav nav ml-auto']) !!}
                      @endif
                    </div>
                    <!-- <form class="form-inline collapse navbar-collapse pull-right">
                    <button class="btn btn-outline-success" type="button">Main button</button>
                    </form> -->
                  </nav>
          </div>
            <div class="col-sm-12 mob-nav col-lg-12 hide-desktop">
                  <nav class="navbar navbar-expand-lg" role="navigation">
                    <div class="collapse navbar-collapse nav-primary ml-auto justify-content-md-center"  id="navbarSupportedContent">
                      @if (has_nav_menu('primary_navigation'))
                        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'depth' => 4, 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'navbar-nav nav ml-auto']) !!}
                      @endif
                    </div>
                    <!-- <form class="form-inline collapse navbar-collapse pull-right">
                    <button class="btn btn-outline-success" type="button">Main button</button>
                    </form> -->
                  </nav>
              </div>
        </div>
      </div>
    </header>
</div>
<!-- <?php //if( get_field('site_notice', 'option') ): ?>
  <div class="container site-cont">
    <div class="site-notice">
      <?php //the_field('site_notice', 'option'); ?>
    </div>
  </div>
 <?php //endif; ?> -->
