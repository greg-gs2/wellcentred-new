<!-- <div class="pre-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-lg-12">
         <img class="footer-logo" src="<?php //the_field('logo_alternative', 'option'); ?>" alt="{{ get_bloginfo('name', 'display') }}">
       </div>
       <div class="col-sm-12 col-lg-12 social-footer">
          @include('partials.builder-elements.social')
      </div>
    </div>
  </div>
</div> -->
<footer class="content-info footer container">
  <div class="container footer-widget-area">
    <div class="row">
      <div class="col-sm-12 col-md-3">
        @php dynamic_sidebar('sidebar-footer-1') @endphp
      </div>
      <div class="col-sm-12 col-md-3">
        @php dynamic_sidebar('sidebar-footer-2') @endphp
      </div>
      <div class="col-sm-12 col-md-3">
        @php dynamic_sidebar('sidebar-footer-3') @endphp
      </div>
      <div class="col-sm-12 col-md-3">
        @php dynamic_sidebar('sidebar-footer-4') @endphp
      </div>
    </div>
  </div>
</footer>
<div class="footer-copyright">
    <div class="container">
       <div class="row">
          <div class="col-sm-12 col-md-8 footer-left">
            @php dynamic_sidebar('sidebar-footer-copyright') @endphp
          </div>
          <div class="col-sm-12 col-md-4 footer-right">
             @include('partials.builder-elements.social')
          </div>
          
        </div>
      
    </div>
</div>


<?php if( have_rows('footer_tracking_codes') ): ?>
    <?php while( have_rows('footer_tracking_codes') ) : the_row(); ?>
        <?php get_sub_field('code'); ?>
    <?php endwhile; ?>
<?php endif; ?> 
