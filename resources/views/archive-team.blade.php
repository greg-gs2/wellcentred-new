@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <h1>Meet The Team</h1>
  <p>Meet the team at Clinica Medica. We only hire the best to make your experience at Clinica Medica the best it can be. With our state of the art hair, beauty and aesthetics clinic, we have treatments for everyone.</p>

<div class="row">
  @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile
</div>

  {{--{!! get_the_posts_navigation() !!}--}}
  <?php numeric_posts_nav(); ?>
@endsection
