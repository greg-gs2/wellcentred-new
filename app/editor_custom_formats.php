<?php

function add_style_select_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {  

    $style_formats = array(  
        // These are the custom styles
        array(  
            'title' => 'Button',  
            'block' => 'div',  
            'classes' => 'inline-button',
        ),  
        array(  
            'title' => 'Contact Modal Button',  
            'block' => 'div',  
            'classes' => 'contactmodal',
        ), 
        array(  
            'title' => 'Home Demo Modal Button',  
            'block' => 'div',  
            'classes' => 'homedemo',
        ), 
        array(  
            'title' => 'Hire Modal Button',  
            'block' => 'div',  
            'classes' => 'hiremodal',
        ),  
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
    
    return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );
