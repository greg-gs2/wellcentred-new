<?php
/**
 * Custom login Page
 */

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
                background-image: url(<?php the_field('logo_alternative', 'option'); ?>);
                height: 100px;
                width: auto;
                background-size: contain;
                background-repeat: no-repeat;
        }
        .login{
            background-image: url(<?php the_field('login_background', 'option'); ?>);
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            background-color: #fff;
            color:#fff;
        }
        .login form {
            background: transparent!important;
            border: none!important;
        }

        .login #backtoblog a, .login #nav a {
            color: #fff!important;
        }

        .login #login_error, .login .message, .login .success{
            background-color: transparent!important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo');


function my_login_logo2() { ?>
<h2 style="text-align:center;">Welcome To <?php echo get_bloginfo( 'name' ); ?></h2>
<?php }
add_action( 'login_message', 'my_login_logo2', 40);


function new_wp_login_url() {
        return home_url();
}
add_filter('login_headerurl', 'new_wp_login_url');

function new_wp_login_title() {
        return get_option('blogname');
}
add_filter('login_headertext', 'new_wp_login_title');
 